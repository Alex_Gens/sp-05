package ru.kazakov.iteco;

import feign.FeignException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.kazakov.iteco.client.ProjectClient;
import ru.kazakov.iteco.client.TaskClient;
import ru.kazakov.iteco.client.UserClient;
import ru.kazakov.iteco.dto.ProjectDTO;
import ru.kazakov.iteco.dto.TaskDTO;
import ru.kazakov.iteco.dto.UserDTO;
import ru.kazakov.iteco.model.Task;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public class TaskTest {

    @NotNull
    private final String testId = UUID.randomUUID().toString();

    @NotNull
    private final String testName = "Test";

    @NotNull
    private final String url = "http://localhost:8080/";

    @Nullable
    private UserDTO userDto;

    @Nullable
    private TaskDTO testDto;

    @Nullable
    private String sessionId;

    @Before
    public void initTestDto() {
        userDto = new UserDTO();
        userDto.setId("userTestId");
        userDto.setUsername("userTest");
        userDto.setPasswordHash("pass");
        sessionId = UserClient.client(url).login(userDto);
        testDto = new TaskDTO();
        testDto.setDateCreate(new Date());
        testDto.setId(testId);
        testDto.setName(testName);
        testDto.setUserId(userDto.getId());
        TaskClient.client(url).addTask(sessionId, testDto);
    }

    @After
    public void termTestDto() {
        @Nullable TaskDTO dto;
        try {
            dto = TaskClient.client(url).getTask(sessionId, testId);
        } catch (FeignException e) {
            userDto = new UserDTO();
            userDto.setId("userTestId");
            userDto.setUsername("userTest");
            userDto.setPasswordHash("pass");
            sessionId = UserClient.client(url).login(userDto);
            dto = TaskClient.client(url).getTask(sessionId, testId);
        }
        if (dto != null) {
            TaskClient.client(url).deleteTask(sessionId, testId);
        }
        testDto = null;
        UserClient.client(url).logout(sessionId);
        sessionId = null;
        userDto = null;
    }

    @Test(expected = FeignException.class)
    public void secureAccess() {
        UserClient.client(url).logout(sessionId);
        @Nullable final TaskDTO dto = TaskClient.client(url).getTask(sessionId, testId);
    }

    @Test
    public void getAllTasks() {
        @NotNull final TaskDTO test2 = new TaskDTO();
        test2.setName("test2");
        test2.setId(UUID.randomUUID().toString());
        test2.setDateCreate(new Date());
        test2.setUserId("adminTestId");
        TaskClient.client(url).addTask(sessionId, test2);
        @Nullable final List<TaskDTO> dtos = TaskClient.client(url).getAllTask(sessionId);
        Assert.assertNotNull(dtos);
        Assert.assertEquals(1, dtos.size());
        UserClient.client(url).logout(sessionId);
        @NotNull final UserDTO adminDto = new UserDTO();
        adminDto.setId("adminTestId");
        adminDto.setUsername("adminTest");
        adminDto.setPasswordHash("pass");
        @NotNull final String adminSessionId = UserClient.client(url).login(adminDto);
        TaskClient.client(url).deleteTask(adminSessionId, test2.getId());
        UserClient.client(url).logout(adminSessionId);
        sessionId = UserClient.client(url).login(userDto);
    }

    @Test
    public void getTask() {
        @Nullable final TaskDTO dto = TaskClient.client(url).getTask(sessionId, testId);
        Assert.assertNotNull(dto);
        Assert.assertEquals(dto.getId(), testId);
    }

    @Test
    public void addTask() {
        @NotNull final TaskDTO test2 = new TaskDTO();
        test2.setName("test2");
        test2.setId(UUID.randomUUID().toString());
        test2.setDateCreate(new Date());
        Assert.assertNotNull(userDto);
        test2.setUserId(userDto.getId());
        TaskClient.client(url).addTask(sessionId, test2);
        @Nullable final TaskDTO dto = TaskClient.client(url).getTask(sessionId, test2.getId());
        Assert.assertNotNull(dto);
        Assert.assertEquals(test2.getId(), dto.getId());
        TaskClient.client(url).deleteTask(sessionId, test2.getId());
    }

    @Test
    public void deleteTask() {
        TaskClient.client(url).deleteTask(sessionId, testId);
        @Nullable final TaskDTO dto = TaskClient.client(url).getTask(sessionId, testId);
        Assert.assertNull(dto);
    }

    @Test
    public void updateTask() {
        Assert.assertNotNull(testDto);
        @NotNull final String updatedName = "TestUpdated";
        testDto.setName(updatedName);
        TaskClient.client(url).updateTask(sessionId, testDto);
        @Nullable final TaskDTO dto = TaskClient.client(url).getTask(sessionId, testId);
        Assert.assertNotNull(dto);
        Assert.assertEquals(dto.getName(), updatedName);
    }

}
