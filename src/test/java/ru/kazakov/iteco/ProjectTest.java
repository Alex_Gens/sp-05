package ru.kazakov.iteco;

import feign.FeignException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.util.Base64Utils;
import ru.kazakov.iteco.client.ProjectClient;
import ru.kazakov.iteco.client.UserClient;
import ru.kazakov.iteco.dto.ProjectDTO;
import ru.kazakov.iteco.dto.UserDTO;
import ru.kazakov.iteco.model.Project;

import javax.validation.constraints.Null;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class ProjectTest {

    @NotNull
    private final String testId = UUID.randomUUID().toString();

    @NotNull
    private final String testName = "Test";

    @NotNull
    private final String url = "http://localhost:8080/";

    @Nullable
    private UserDTO userDto;

    @Nullable
    private ProjectDTO testDto;

    @Nullable
    private String sessionId;

    @Before
    public void initTestDto() {
        userDto = new UserDTO();
        userDto.setId("userTestId");
        userDto.setUsername("userTest");
        userDto.setPasswordHash("pass");
        sessionId = UserClient.client(url).login(userDto);
        testDto = new ProjectDTO();
        testDto.setDateCreate(new Date());
        testDto.setId(testId);
        testDto.setName(testName);
        testDto.setUserId(userDto.getId());
        ProjectClient.client(url).addProject(sessionId, testDto);
    }

    @After
    public void termTestDto() {
        @Nullable ProjectDTO dto;
        try {
           dto = ProjectClient.client(url).getProject(sessionId, testId);
        } catch (FeignException e) {
            userDto = new UserDTO();
            userDto.setId("userTestId");
            userDto.setUsername("userTest");
            userDto.setPasswordHash("pass");
            sessionId = UserClient.client(url).login(userDto);
            dto = ProjectClient.client(url).getProject(sessionId, testId);
        }
        if (dto != null) {
            ProjectClient.client(url).deleteProject(sessionId, testId);
        }
        testDto = null;
        UserClient.client(url).logout(sessionId);
        sessionId = null;
        userDto = null;
    }

    @Test(expected = FeignException.class)
    public void secureAccess() {
        UserClient.client(url).logout(sessionId);
        @Nullable final ProjectDTO dto = ProjectClient.client(url).getProject(sessionId, testId);
    }

    @Test
    public void getAllProjects() {
        @NotNull final ProjectDTO test2 = new ProjectDTO();
        test2.setName("test2");
        test2.setId(UUID.randomUUID().toString());
        test2.setDateCreate(new Date());
        test2.setUserId("adminTestId");
        ProjectClient.client(url).addProject(sessionId, test2);
        @Nullable final List<ProjectDTO> dtos = ProjectClient.client(url).getAllProject(sessionId);
        Assert.assertNotNull(dtos);
        Assert.assertEquals(1, dtos.size());
        UserClient.client(url).logout(sessionId);
        @NotNull final UserDTO adminDto = new UserDTO();
        adminDto.setId("adminTestId");
        adminDto.setUsername("adminTest");
        adminDto.setPasswordHash("pass");
        @NotNull final String adminSessionId = UserClient.client(url).login(adminDto);
        ProjectClient.client(url).deleteProject(adminSessionId, test2.getId());
        UserClient.client(url).logout(adminSessionId);
        sessionId = UserClient.client(url).login(userDto);
    }

    @Test
    public void getProject() {
        @Nullable final ProjectDTO dto = ProjectClient.client(url).getProject(sessionId, testId);
        Assert.assertNotNull(dto);
        Assert.assertEquals(dto.getId(), testId);
    }

    @Test
    public void addProject() {
        @NotNull final ProjectDTO test2 = new ProjectDTO();
        test2.setName("test2");
        test2.setId(UUID.randomUUID().toString());
        test2.setDateCreate(new Date());
        Assert.assertNotNull(userDto);
        test2.setUserId(userDto.getId());
        ProjectClient.client(url).addProject(sessionId, test2);
        @Nullable final ProjectDTO dto = ProjectClient.client(url).getProject(sessionId, test2.getId());
        Assert.assertNotNull(dto);
        Assert.assertEquals(test2.getId(), dto.getId());
        ProjectClient.client(url).deleteProject(sessionId, test2.getId());
    }

    @Test
    public void deleteProject() {
        ProjectClient.client(url).deleteProject(sessionId, testId);
        @Nullable final ProjectDTO dto = ProjectClient.client(url).getProject(sessionId, testId);
        Assert.assertNull(dto);
    }

    @Test
    public void updateProject() {
        Assert.assertNotNull(testDto);
        @NotNull final String updatedName = "TestUpdated";
        testDto.setName(updatedName);
        ProjectClient.client(url).updateProject(sessionId, testDto);
        @Nullable final ProjectDTO dto = ProjectClient.client(url).getProject(sessionId, testId);
        Assert.assertNotNull(dto);
        Assert.assertEquals(dto.getName(), updatedName);
    }

}
