package ru.kazakov.iteco.api.service;

import org.jetbrains.annotations.Nullable;

public interface ISecurityService {

    public String findLoggedInUserName();

    public void logIn(
            @Nullable final String login,
            @Nullable final String password
    );

}
