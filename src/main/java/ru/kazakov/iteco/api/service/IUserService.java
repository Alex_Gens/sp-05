package ru.kazakov.iteco.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.model.User;
import java.util.List;

public interface IUserService {

    @NotNull
    public User create(@Nullable final String login,
                       @Nullable final  String password
    ) throws Exception;

    @NotNull
    public User save(@Nullable final User user) throws Exception;

    @Nullable
    public User findById(@Nullable final String id) throws Exception;

    @Nullable
    public User findByUsername(@Nullable final String username) throws Exception;

    @NotNull
    public List<User> findAll();

    @NotNull
    public List<User> findAllByOrderByDateCreate();

    public boolean existsById(@Nullable final String id) throws Exception;

    public void deleteById(@Nullable final String id) throws Exception;

    public void deleteAll();

}
