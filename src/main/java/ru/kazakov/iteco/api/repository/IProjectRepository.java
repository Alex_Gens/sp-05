package ru.kazakov.iteco.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.repository.PagingAndSortingRepository;
import ru.kazakov.iteco.model.Project;
import java.util.List;

public interface IProjectRepository extends PagingAndSortingRepository<Project, String> {

    @NotNull
    public List<Project> findAllByUserIdOrderByDateCreate(@NotNull final String userId);

    @Nullable
    public Project findByIdAndUserId(@NotNull final String id,
                                     @NotNull final String userId);

}
