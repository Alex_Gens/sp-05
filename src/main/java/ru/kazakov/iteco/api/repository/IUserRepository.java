package ru.kazakov.iteco.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.repository.CrudRepository;
import ru.kazakov.iteco.model.User;
import java.util.List;

public interface IUserRepository extends CrudRepository<User, String> {

    public User findByUsername(@NotNull final String username);

    public List<User> findAllByOrderByDateCreate();

}
