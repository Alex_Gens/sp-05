package ru.kazakov.iteco.api.repository;

import org.springframework.data.repository.CrudRepository;
import ru.kazakov.iteco.model.Role;

public interface IRoleRepository extends CrudRepository<Role, String> {}
