package ru.kazakov.iteco.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.repository.PagingAndSortingRepository;
import ru.kazakov.iteco.model.Task;

import java.util.List;

public interface ITaskRepository extends PagingAndSortingRepository<Task, String> {

    @NotNull
    public List<Task> findAllByUserIdOrderByDateCreate(@NotNull final String userId);

    @Nullable
    public Task findByIdAndUserId(@NotNull final String id,
                                  @NotNull final String userId);

}

