package ru.kazakov.iteco.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kazakov.iteco.api.repository.IUserRepository;
import ru.kazakov.iteco.model.Role;
import ru.kazakov.iteco.model.User;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserDetailsServiceBean implements UserDetailsService {

    @NotNull
    @Autowired
    private IUserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(@NotNull final String username) throws UsernameNotFoundException {
        @Nullable final User user = findByUsername(username);
        if (user == null) throw new UsernameNotFoundException("User not found.");
        org.springframework.security.core.userdetails.User.UserBuilder builder = null;
        builder = org.springframework.security.core.userdetails.User.withUsername(username);
        builder.password(user.getPassword());
        final List<Role> userRoles = user.getRoles();
        final List<String> roles = new ArrayList<>();
        for (Role role: userRoles) roles.add(role.toString());
        builder.roles(roles.toArray(new String[] {}));
        return builder.build();
    }
    private User findByUsername(@NotNull final String username) {return userRepository.findByUsername(username);}

}
