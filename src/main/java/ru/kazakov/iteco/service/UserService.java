package ru.kazakov.iteco.service;

import com.google.common.collect.Lists;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kazakov.iteco.api.repository.IUserRepository;
import ru.kazakov.iteco.api.service.IRoleService;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.enumeration.RoleType;
import ru.kazakov.iteco.model.Role;
import ru.kazakov.iteco.model.User;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class UserService implements IUserService {

    @NotNull
    @Autowired
    private IUserRepository repository;

    @NotNull
    @Autowired
    private IRoleService roleService;

    @NotNull
    @Autowired
    private PasswordEncoder encoder;

    @NotNull
    public User create(@Nullable final String username,
                       @Nullable final  String password
    ) throws Exception {
        if (username == null || username.isEmpty()) throw new Exception();
        if (password == null || password.isEmpty()) throw new Exception();
        @NotNull final User user = new User();
        user.setUsername(username);
        @NotNull final String hashedPassword = encoder.encode(password);
        user.setPassword(hashedPassword);
        repository.save(user);
        @NotNull final Role role = new Role();
        role.setUser(user);
        role.setRole(RoleType.USER);
        roleService.save(role);
        return user;
    }

    @NotNull
    public User save(@Nullable final User user) throws Exception {
        if(user == null) throw new Exception();
        return repository.save(user);
    }

    @Nullable
    public User findById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        return repository.findById(id).orElse(null);
    }

    @Nullable
    public User findByUsername(@Nullable final String username) throws Exception {
        if (username == null || username.isEmpty()) throw new Exception();
        return repository.findByUsername(username);
    }

    @NotNull
    public List<User> findAll() {return Lists.newArrayList(repository.findAll());}

    @NotNull
    public List<User> findAllByOrderByDateCreate() {
        return repository.findAllByOrderByDateCreate();
    }

    public boolean existsById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        return repository.existsById(id);
    }

    public void deleteById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        repository.deleteById(id);
    }

    public void deleteAll() {repository.deleteAll();}

    @PostConstruct
    private void init() throws Exception {
        if (findByUsername("user") == null) {
            create("user", "pass");
        }
    }

    @PostConstruct
    private void initAdmin() throws Exception {
        if (findByUsername("admin") == null) {
            @NotNull final User user = new User();
            user.setUsername("admin");
            @NotNull final String hashedPassword = encoder.encode("pass");
            user.setPassword(hashedPassword);
            repository.save(user);
            @NotNull final Role role = new Role();
            role.setUser(user);
            role.setRole(RoleType.ADMINISTRATOR);
            roleService.save(role);
        }
    }

    @PostConstruct
    private void initTestUsers() throws Exception {
        if (findByUsername("userTest") == null) {
            @NotNull final User user = new User();
            user.setId("userTestId");
            user.setUsername("userTest");
            @NotNull final String hashedPassword = encoder.encode("pass");
            user.setPassword(hashedPassword);
            repository.save(user);
            @NotNull final Role role = new Role();
            role.setUser(user);
            role.setRole(RoleType.USER);
            roleService.save(role);
        }
        if (findByUsername("adminTest") == null) {
            @NotNull final User user = new User();
            user.setId("adminTestId");
            user.setUsername("adminTest");
            @NotNull final String hashedPassword = encoder.encode("pass");
            user.setPassword(hashedPassword);
            repository.save(user);
            @NotNull final Role role = new Role();
            role.setUser(user);
            role.setRole(RoleType.ADMINISTRATOR);
            roleService.save(role);
        }
    }

}
