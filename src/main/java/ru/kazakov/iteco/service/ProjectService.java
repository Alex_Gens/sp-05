package ru.kazakov.iteco.service;

import com.google.common.collect.Lists;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kazakov.iteco.api.repository.IProjectRepository;
import ru.kazakov.iteco.api.service.IProjectService;
import ru.kazakov.iteco.model.Project;
import java.util.List;

@Service
@Transactional
public class ProjectService implements IProjectService {

    @NotNull
    @Autowired
    private IProjectRepository repository;

    public Project save(@Nullable final Project s) throws Exception {
        if(s == null) throw new Exception();
        return repository.save(s);
    }

    @Nullable
    public Project findById(@Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception();
        return repository.findById(id).orElse(null);
    }

    @Nullable
    public Project findOne(@Nullable final String id,
                           @Nullable final String userId) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception();
        if(userId == null || userId.isEmpty()) throw new Exception();
        return repository.findByIdAndUserId(id, userId);
    }

    public boolean existsById(@Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception();
        return repository.existsById(id);
    }

    public List<Project> findAll() {return Lists.newArrayList(repository.findAll());}

    public List<Project> findAll(@Nullable final Sort sort) throws Exception {
        if(sort == null) throw new Exception();
        return Lists.newArrayList(repository.findAll(sort));
    }

    @NotNull
    public List<Project> findAllByUserIdOrderByDateCreate(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        return repository.findAllByUserIdOrderByDateCreate(userId);
    }

    public long count() {return repository.count();}

    public void deleteById(@Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception();
        repository.deleteById(id);
    }

    public void delete(@Nullable final Project project) throws Exception {
        if(project == null) throw new Exception();
        repository.delete(project);
    }

    public void deleteAll() {repository.deleteAll();}

}
