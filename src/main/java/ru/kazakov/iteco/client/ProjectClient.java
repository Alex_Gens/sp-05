package ru.kazakov.iteco.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.kazakov.iteco.dto.ProjectDTO;
import java.util.List;

@FeignClient("project")
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public interface ProjectClient {

    static ProjectClient client(@NotNull final String baseUrl) {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ProjectClient.class, baseUrl);
        }

    @GetMapping("/projects")
    List<ProjectDTO> getAllProject(@RequestHeader("cookie") @NotNull final String header);

    @GetMapping(value = "/projects/{id}")
    ProjectDTO getProject(@RequestHeader("cookie") @NotNull final String header,
                          @PathVariable(value = "id") @Nullable final String id);

    @PostMapping("/projects")
    public ProjectDTO addProject(@RequestHeader("cookie") @NotNull final String header,
                                 @Nullable final ProjectDTO dto);

    @DeleteMapping("/projects/{id}")
    public void deleteProject(@RequestHeader("cookie") @NotNull final String sessionId,
                              @PathVariable(value = "id") @Nullable final String id);

    @PutMapping("/projects")
    public void updateProject(@RequestHeader("cookie") @NotNull final String header,
                              @Nullable final ProjectDTO dto);

}
