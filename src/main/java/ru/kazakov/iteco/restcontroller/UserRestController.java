package ru.kazakov.iteco.restcontroller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.dto.UserDTO;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/users",
                produces =  {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
public class UserRestController {

    @NotNull
    @Autowired
    IUserService userService;

    @NotNull
    @Autowired
    private AuthenticationManager manager;

    @NotNull
    @Autowired
    private PasswordEncoder encoder;

    @PostMapping(value = "/login_user")
    public String login(
            @RequestBody final UserDTO dto,
            @NotNull final HttpServletRequest request
    ) {
        final String username = dto.getUsername();
        final String password = dto.getPasswordHash();
        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
        final Authentication authentication = manager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return "JSESSIONID=" + request.getSession().getId();
    }

    @PostMapping(value = "/logout_user")
    public void logout() {SecurityContextHolder.getContext().setAuthentication(null);}

}
